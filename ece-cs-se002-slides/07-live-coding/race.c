// Lin Tan 
// Expected behavior: print the total number of threads. 
// This program contains data races. 
// Run gcc -pthread -g race.c to compile
// Run valgrind --tool=helgrind ./a.out to detect the races

#include <stdio.h>
#include <pthread.h>

int counter = 0; // shared variable

void * func() {
        int tmp;

        tmp = counter;
        tmp++;
        counter = tmp;
}

int main() {
        pthread_t thread1, thread2;
        pthread_create (&thread1, 0, func, 0);
        pthread_create (&thread2, 0, func, 0);
        pthread_join (thread1, 0);
        pthread_join (thread2, 0);
        printf("%d\n", counter);
        return 0;
}

