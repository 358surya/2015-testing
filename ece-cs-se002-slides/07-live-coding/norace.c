// Lin Tan 
// Expected behavior: print the total number of threads. 
// This program doesn't contain a data race, but it still contains a bug. 
// Run gcc -pthread -g race.c to compile
//     valgrind --tool=helgrind ./a.out shows that no races are detected. 

#include <stdio.h>
#include <pthread.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int counter = 0; // shared variable

void * func() {
        int tmp;

        pthread_mutex_lock(&mutex1);
        tmp = counter;
        pthread_mutex_unlock(&mutex1);
        sleep(1);
        tmp++;
        pthread_mutex_lock(&mutex1);
        counter = tmp;
        pthread_mutex_unlock(&mutex1);
}

int main() {
        pthread_t thread1, thread2;
        pthread_create (&thread1, 0, func, 0);
        pthread_create (&thread2, 0, func, 0);
        pthread_join (thread1, 0);
        pthread_join (thread2, 0);
        printf("%d\n", counter);
        return 0;
}
